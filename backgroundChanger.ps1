$bgURL = "https://gitlab.com/itshorty/intunewallpaperchanger/-/raw/master/background.jpg"
$tempDir = "$env:temp/bgchanger"


# From: https://www.joseespitia.com/2017/09/15/set-wallpaper-powershell-function/
Function Set-WallPaper {     
    param (
        [parameter(Mandatory=$True)]
        # Provide path to image
        [string]$Image,
        # Provide wallpaper style that you would like applied
        [parameter(Mandatory=$False)]
        [ValidateSet('Fill', 'Fit', 'Stretch', 'Tile', 'Center', 'Span')]
        [string]$Style
    )
     
    $WallpaperStyle = Switch ($Style) {
      
        "Fill" {"10"}
        "Fit" {"6"}
        "Stretch" {"2"}
        "Tile" {"0"}
        "Center" {"0"}
        "Span" {"22"}
      
    }
     
    If($Style -eq "Tile") {
     
        New-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name WallpaperStyle -PropertyType String -Value $WallpaperStyle -Force
        New-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name TileWallpaper -PropertyType String -Value 1 -Force
     
    }
    Else {
     
        New-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name WallpaperStyle -PropertyType String -Value $WallpaperStyle -Force
        New-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name TileWallpaper -PropertyType String -Value 0 -Force
     
    }
     
    Add-Type -TypeDefinition @" 
    using System; 
    using System.Runtime.InteropServices;
      
    public class Params
    { 
        [DllImport("User32.dll",CharSet=CharSet.Unicode)] 
        public static extern int SystemParametersInfo (Int32 uAction, 
                                                       Int32 uParam, 
                                                       String lpvParam, 
                                                       Int32 fuWinIni);
    }
"@ 
      
        $SPI_SETDESKWALLPAPER = 0x0014
        $UpdateIniFile = 0x01
        $SendChangeEvent = 0x02
      
        $fWinIni = $UpdateIniFile -bor $SendChangeEvent
      
        $ret = [Params]::SystemParametersInfo($SPI_SETDESKWALLPAPER, 0, $Image, $fWinIni)
    }


if ( -not (Test-Path $tempDir)) {
    $null = New-Item -Type Directory -Path $tempDir
}
$onlineSHA = $null
$onlineSHA = Invoke-RestMethod -Method GET "$bgURL.sha256"
$onlineSHA = $onlineSHA.Trim()

if ($onlineSHA -eq $null){
    Write-Error "Error durring download of SHA sum from $bgUrl.sha256"
    exit;
}

$update = $true

if ( Test-Path "$tempDir\background.jpg" ){
    # Compare SHA Sums
    $currentSHA = $(Get-FileHash -Algorithm SHA256 -Path "$tempDir\background.jpg").Hash
    if ("$currentSHA" -eq "$onlineSHA"){
        Write-Host "Nothing to do - SHA matching"
        $update = $false
    }
}

if ($update){
    # Download new background and set it
    Write-Host "Updateing Background..."
    Invoke-WebRequest -Uri $bgURL -OutFile "$tempDir\background.jpg"
    Set-Wallpaper -Image "$tempDir\background.jpg" -Style Fill
}


# compare
# update 


